import React from 'react';
import Titol from './Titol';
import Quadricula from "./Quadricula";
import Taula from "./Taula";

import "./App.css";
import Calendario from './Calendario';


const MOTOS = [
  {
    marca: "honda",
    model: "scoopy"
  },
  {
    marca: "honda",
    model: "cbr125"
  },
  {
    marca: "yamaha",
    model: "fzr1000"
  },
  {
    marca: "gilera",
    model: "nexus"
  },
  {
    marca: "aprilia",
    model: "mana"
  },
]


function App() {
  return (
    <>
      <Titol nom="Mataró" salutacio="hola" />
      <Quadricula files={4} columnes={6} />
      <Taula dades={MOTOS} />
      <div className="prueba">
        <Calendario mes={5} any={2021}/>
      </div>
    </>
  );
}

export default App;
