import React from "react";
import moment from 'moment'
import "./App.css";

function Calendario(props){
    var data = props.mes + " 1 "+ props.any
    var dayInicio = moment(data).format("ddd")

    var days=["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ];
    var dias=["lun", "mar", "mie", "jue", "vie", "sab", "dom" ];
    var diaInicio = days.indexOf(dayInicio)
    var numerDias = new Date(props.any, props.mes, 0).getDate();
    
    let x = [];
    for (let d=0; d<7;d++){
        x.push(<div className="celda">{dias[d]}</div>)
    }

    for (let y=0; y<numerDias+diaInicio; y++){
        if(y<diaInicio){
            x.push(<div className="celdaBlanca" ></div>)
        }else{
            x.push(<div className="celda">{y-(diaInicio-1)}</div>)
        }
    }
    return x;
};

export default Calendario;