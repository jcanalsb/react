import React, {useState} from 'react';


function Titol(props){

    const [vermell, setVermell] = useState(false);

    function canviColor(){
        setVermell(!vermell);
        // console.log("canviant color... ", vermell)
    }

    if (vermell) {
        return <h1 onClick={canviColor} className="vermell">{props.salutacio} {props.nom}</h1>;
    } else {
        return <h1 onClick={canviColor}>{props.salutacio} {props.nom}</h1>;
    }
    
  }

  export default Titol;

  